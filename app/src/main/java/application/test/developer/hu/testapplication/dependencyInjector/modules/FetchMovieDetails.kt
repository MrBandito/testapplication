package application.test.developer.hu.testapplication.dependencyInjector.modules

import application.test.developer.hu.testapplication.requests.FetchMovieDetails
import dagger.Module
import dagger.Provides
import dagger.Reusable
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

@Module
class FetMovieDetailsModule
{
    @Provides
    @Reusable
    internal fun providePostApi(retrofit: Retrofit): FetchMovieDetails {
        return retrofit.create(FetchMovieDetails::class.java)
    }

    /**
     * Provides the Retrofit object.
     * @return the Retrofit object
     */
    @Provides
    @Reusable
    internal fun provideRetrofitSearchMovie(): Retrofit {
        return Retrofit.Builder()
                .baseUrl("https://api.themoviedb.org/3/search/movie")
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build()
    }
}