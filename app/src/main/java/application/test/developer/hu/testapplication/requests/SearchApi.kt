package application.test.developer.hu.testapplication.requests

import application.test.developer.hu.testapplication.entity.MovieDetails
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.QueryMap


interface SearchApi {
    @GET("search/")
    fun searchMovie(@Query("movie")apiKey : String, @QueryMap options : Map<String, String> ): Call<List<MovieDetails>>

    @GET("movie/{id}")
    fun searchMovieDetails(@Path("id") movieId : Int,@Query("movie")apiKey : String): Call<List<MovieDetails>>
}