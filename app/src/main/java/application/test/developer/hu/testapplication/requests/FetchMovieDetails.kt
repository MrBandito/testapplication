package application.test.developer.hu.testapplication.requests

import dagger.Provides
import dagger.Reusable
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory


class  FetchMovieDetails
{
    private object Holder { val INSTANCE = FetchMovieDetails() }
    companion object {
        val getInstance: FetchMovieDetails by lazy { Holder.INSTANCE }
    }
    private var retrofitSearch : Retrofit = Retrofit.Builder()
           .baseUrl("https://api.themoviedb.org/3/")
            .addConverterFactory(MoshiConverterFactory.create())
           .build()
    fun SearchMovie(): SearchApi =  retrofitSearch.create(SearchApi::class.java)
}