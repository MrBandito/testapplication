package application.test.developer.hu.testapplication.dependencyInjector.components


import dagger.Component
import dagger.android.AndroidInjector
import application.test.developer.hu.testapplication.application.*
import application.test.developer.hu.testapplication.dependencyInjector.modules.ApplicationModule
import application.test.developer.hu.testapplication.requests.FetchMovieDetails
import application.test.developer.hu.testapplication.dependencyInjector.bindings.ActivityBindings
import javax.inject.Singleton

/**
 * Created by farkasa on 2018.02.13..
 */
@Singleton
@Component(modules = [(ApplicationModule::class),
    (FetchMovieDetails::class),
    (ActivityBindings::class)])
interface  ApplicationComponent : AndroidInjector<MovieApplication> {


    @Component.Builder
    abstract  class Builder : AndroidInjector.Builder<MovieApplication>()
}