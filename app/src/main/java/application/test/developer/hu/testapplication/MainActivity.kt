package application.test.developer.hu.testapplication

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import application.test.developer.hu.testapplication.adapter.*
import application.test.developer.hu.testapplication.requests.FetchMovieDetails
import com.jakewharton.rxbinding2.widget.textChanges
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

       override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val recylerAdapter = ReactiveRecylerAdapter()
        rwMoviesList.adapter = recylerAdapter
        rwMoviesList.layoutManager = LinearLayoutManager(this)


        etSearch.textChanges().doOnNext { t ->
            val params : Map<String,String> = emptyMap()
            params.plus(Pair("query","Padlogaz"))
          val test =  FetchMovieDetails.getInstance.SearchMovie().searchMovie("43a7ea280d085bd0376e108680615c7f",params)

            recylerAdapter.filter.filter(t.trim().toString()) }
                .observeOn(Schedulers.newThread())
                .subscribe()

    }

}
