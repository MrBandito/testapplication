package application.test.developer.hu.testapplication.dependencyInjector.bindings

import application.test.developer.hu.testapplication.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector



/**
 * Created by farkasa on 2018.02.26..
 */
@Module
abstract class ActivityBindings {

        @ContributesAndroidInjector
        abstract fun bindMainActivity(): MainActivity
}
