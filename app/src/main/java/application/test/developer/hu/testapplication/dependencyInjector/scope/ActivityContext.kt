package application.test.developer.hu.testapplication.dependencyInjector.scope

import javax.inject.Qualifier
/**
 * Created by farkasa on 2018.02.13..
 */

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class ActivityContext
