package application.test.developer.hu.testapplication.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.movielistitem.view.*
class AnswerListDataViewHolder (itemView: View?) : RecyclerView.ViewHolder(itemView) {
    val image  = itemView?.imgMoviePhoto
    val budget  = itemView?.tvBudget
}
