package application.test.developer.hu.testapplication.adapter

import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.support.annotation.NonNull
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import application.test.developer.hu.testapplication.R
import application.test.developer.hu.testapplication.entity.MovieDetails

class ReactiveRecylerAdapter : RecyclerView.Adapter<AnswerListDataViewHolder>(), Filterable {


    var currentList: MutableList<MovieDetails>  = mutableListOf()
    private lateinit var customfilter : CustomFilter

    init {
        customfilter = CustomFilter(this,currentList.toMutableList())
    }

    override fun onCreateViewHolder(@NonNull parent: ViewGroup, pViewType: Int): AnswerListDataViewHolder {
        return AnswerListDataViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.movielistitem, parent, false))
    }

    override fun onBindViewHolder(holder: AnswerListDataViewHolder, position: Int) {
        val item = currentList[position]
        holder.budget?.text = item.budget
        holder.image?.setImageDrawable(item.image)
    }

    override fun getItemCount(): Int {
        return currentList.size
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)

    }
    /**
     *
     * Returns a filter that can be used to constrain data with a filtering
     * pattern.
     *
     *
     * This method is usually implemented by [android.widget.Adapter]
     * classes.
     *
     * @return a filter used to constrain data
     */
    override fun getFilter(): Filter {
        return customfilter
    }
    private class CustomFilter constructor(private val adapter : ReactiveRecylerAdapter,val currentList : MutableList<MovieDetails>) : Filter()
    {
        val filteredList : MutableList<MovieDetails> = mutableListOf()
        /**
         *
         * Invoked in a worker thread to filter the data according to the
         * constraint. Subclasses must implement this method to perform the
         * filtering operation. Results computed by the filtering operation
         * must be returned as a [android.widget.Filter.FilterResults] that
         * will then be published in the UI thread through
         * [.publishResults].
         *
         *
         * **Contract:** When the constraint is null, the original
         * data must be restored.
         *
         * @param constraint the constraint used to filter the data
         * @return the results of the filtering operation
         *
         * @see .filter
         * @see .publishResults
         * @see android.widget.Filter.FilterResults
         */
        override fun performFiltering(constraint: CharSequence?): FilterResults {

            filteredList.clear();
            val results = Filter.FilterResults()
            if (constraint.isNullOrEmpty()) {
                filteredList.addAll(currentList)
            } else {
                val filterPattern = constraint.toString().toLowerCase().trim()
                for (movieData in currentList) {
                    if(movieData.title.contains(filterPattern)) {
                        filteredList += movieData
                    }
                }
            }
            System.out.println("Count Number " + filteredList.size)
            results.values = filteredList
            results.count = filteredList.size

            return results
        }

        /**
         *
         * Invoked in the UI thread to publish the filtering results in the
         * user interface. Subclasses must implement this method to display the
         * results computed in [.performFiltering].
         *
         * @param constraint the constraint used to filter the data
         * @param results the results of the filtering operation
         *
         * @see .filter
         * @see .performFiltering
         * @see android.widget.Filter.FilterResults
         */
        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            adapter.currentList = results?.values as MutableList<MovieDetails>
            adapter.notifyDataSetChanged()
        }

    }

}