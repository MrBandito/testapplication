package application.test.developer.hu.testapplication.entity

import android.graphics.drawable.Drawable
import android.media.Image
import java.util.*

data class MovieDetails
(val image : Drawable,
 val budget : String,
val title : String,
 val releaseDate : Date
)