package application.test.developer.hu.testapplication.dependencyInjector.scope

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class FragmentContext