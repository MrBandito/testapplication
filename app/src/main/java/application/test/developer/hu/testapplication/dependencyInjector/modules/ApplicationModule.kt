package application.test.developer.hu.testapplication.dependencyInjector.modules

import android.app.Application
import application.test.developer.hu.testapplication.application.MovieApplication
import application.test.developer.hu.testapplication.entity.MovieDetails
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

/**
 * Created by farkasa on 2018.02.13..
 */
@Module(includes = [(AndroidInjectionModule::class)])
abstract class ApplicationModule  {
    @Binds
    @Singleton
    abstract  fun  application(application: MovieApplication) :Application
}